class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    seconds = @seconds % 60
    minutes = (@seconds / 60).floor
    hours = (minutes / 60).floor
    "#{zero_needed?(hours)}:#{zero_needed?(minutes%60)}:#{zero_needed?(seconds)}"
  end

  private

  def zero_needed?(digit)
    if digit.to_s.length == 1
      return "0#{digit}"
    else
      return "#{digit}"
    end
  end

end
