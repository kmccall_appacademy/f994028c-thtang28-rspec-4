class Friend
  def greeting(name = nil)
    unless name.nil?
      return "Hello, #{name}!"
    end
    "Hello!"
  end
end
