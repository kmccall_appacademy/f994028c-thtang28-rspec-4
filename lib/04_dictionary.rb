class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(options = {})
    if options.is_a? String
      @entries[options] = nil
      return
    end
    @entries = @entries.merge(options)
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    @entries.include?(keyword)
  end

  def find(keyword)
    res = {}
    words = @entries.keys
    words.each do |word|
      if word.include?(keyword)
        res[word] = @entries[word]
      end
    end
    res
  end

  def printable
    res = []
    keys = keywords
    keys.each do |key|
      # res << "[#{key}] \"#{@entries[key]}\"\n"
      res << %Q{[#{key}] "#{@entries[key]}"}
    end
    res.join("\n")
  end
end
