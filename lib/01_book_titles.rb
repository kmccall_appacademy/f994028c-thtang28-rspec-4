class Book

  def initialize
  end

  def title
    @title
  end

  def title=(title)
    first_word = title.split[0].capitalize
    no_cap = CONJUNCTIONS + PREPOSITIONS + ARTICLES
    if title.split.length == 1
      @title = first_word
      return
    else
      other_words = title.split[1..-1].map do |word|
        no_cap.include?(word) ? word : word.capitalize
      end.join(" ")
    end
    @title = "#{first_word} #{other_words}"
  end

  private

  CONJUNCTIONS = [
    "and",
    "or",
    "but",
    "nor",
  ]

  PREPOSITIONS = [
    "in",
    "of"
  ]

  ARTICLES = [
    "a",
    "an",
    "the"
  ]
end
