class Temperature

  def initialize(options = {})
    @options = options
  end

  def in_fahrenheit
    key = @options.keys[0]
    return @options[key] if key == :f
    return ftoc(@options[key]) if key == :c
  end

  def in_celsius
    key = @options.keys[0]
    return @options[key] if key == :c
    return ctof(@options[key]) if key == :f
  end

  def self.from_celsius(degree)
    Temperature.new({c:degree})
  end

  def self.from_fahrenheit(degree)
    Temperature.new({f:degree})
  end

  private

  def ftoc(degree)
    (degree * 1/Confac) + 32
  end

  def ctof(degree)
    (degree - 32) * Confac
  end

  Confac = 5/9.to_f
end

class Celsius < Temperature
  attr_accessor :degree
  def initialize(degree)
    @options = {c:degree}
  end
end

class Fahrenheit < Temperature
  def initialize(degree)
    @options = {f:degree}
  end
end
